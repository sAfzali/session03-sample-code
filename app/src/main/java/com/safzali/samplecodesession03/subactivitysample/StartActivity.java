package com.safzali.samplecodesession03.subactivitysample;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.safzali.samplecodesession03.R;
import com.safzali.samplecodesession03.utils.PublicMethods;

public class StartActivity extends AppCompatActivity implements View.OnClickListener {
    TextView textDetailsShow;
    int subRequestCode = 1000;
    String dataResult = "Data is Empry";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        bind();
    }

    void bind() {
        textDetailsShow = findViewById(R.id.text_details_show);
        findViewById(R.id.get_data).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent(StartActivity.this, SubActivity.class);
        startActivityForResult(intent, subRequestCode);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == subRequestCode) {
            if (resultCode == Activity.RESULT_OK) {
                textDetailsShow.setText(data.getStringExtra("value"));
            } else
                PublicMethods.toast(this, dataResult);
        }
    }
}
