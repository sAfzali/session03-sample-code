package com.safzali.samplecodesession03.subactivitysample;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.safzali.samplecodesession03.R;

public class SubActivity extends AppCompatActivity implements View.OnClickListener {
    EditText userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);
        bind();
    }

    void bind() {
        userName = findViewById(R.id.user_name);
        findViewById(R.id.set_data).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        String userValue = userName.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("value", userValue);
        if (userValue.length() > 0) {
            setResult(Activity.RESULT_OK, intent);
        } else
            setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }
}
