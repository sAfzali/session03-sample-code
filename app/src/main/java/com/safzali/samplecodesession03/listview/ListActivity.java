package com.safzali.samplecodesession03.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.safzali.samplecodesession03.R;
import com.safzali.samplecodesession03.utils.BaseActivity;

public class ListActivity extends BaseActivity {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        bind();
        String names[] = {
                "Soheil",
                "Alireza",
                "Ahmad",
                "Amirhossein",
                "Akbar",
                "Ali",
                "Mohammad",
                "Niloofar",
                "Alireza",
        };

        UsersListAdapter adapter = new UsersListAdapter(mContext, names);
        listView.setAdapter(adapter);

    }

    void bind() {
        listView = findViewById(R.id.list_view);
    }


}
