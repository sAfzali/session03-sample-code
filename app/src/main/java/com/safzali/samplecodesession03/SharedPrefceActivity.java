package com.safzali.samplecodesession03;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.safzali.samplecodesession03.utils.BaseActivity;
import com.safzali.samplecodesession03.utils.PublicMethods;

public class SharedPrefceActivity extends BaseActivity implements View.OnClickListener {
    EditText userName, mobile;
    String msg = "Data has been saved";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_prefce);
        bind();
        loadData();
    }

    void bind() {
        userName = findViewById(R.id.user);
        mobile = findViewById(R.id.mobile);
        findViewById(R.id.save_data).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        String userValue = userName.getText().toString();
        String mobileValue = mobile.getText().toString();
/*
PublicMethods.saveData(this, "user_name", userValue);
PublicMethods.saveData(this, "mobile", mobileValue);
Hawk.put("user_name", userValue);
Hawk.put("mobile", mobileValue);
*/
        PublicMethods.saveData("user_name", userValue);
        PublicMethods.saveData("mobile", mobileValue);

        userName.setText("");
        mobile.setText("");
    }

    void loadData() {
        String defUserVal = PublicMethods.getData("user_name", "");
        String defMobileVal = PublicMethods.getData("mobile", "");
        if (defUserVal != null)
            userName.setText(defUserVal);
        if (defMobileVal != null)
            mobile.setText(defMobileVal);
    }
}

