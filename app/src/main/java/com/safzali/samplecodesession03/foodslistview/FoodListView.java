package com.safzali.samplecodesession03.foodslistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.safzali.samplecodesession03.R;
import com.safzali.samplecodesession03.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class FoodListView extends BaseActivity {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list_view);
        bind();


    }

    void bind() {
        listView = findViewById(R.id.my_list_view);
        foodModel();
    }


    void foodModel() {
        FoodModel pizza = new FoodModel();
        pizza.setName("pizza");
        pizza.setPrice(25000);
        pizza.setImage("https://media-cdn.tripadvisor.com/media/photo-s/07/13/66/3b/gianni-s-ny-pizza.jpg");
        FoodModel fesenjan = new FoodModel();
        fesenjan.setName("fesenjan");
        fesenjan.setPrice(50000);
        fesenjan.setImage("https://fae-magazine.com/wp-content/uploads/2015/11/Fesenjan-31.jpg");
        FoodModel mushroomBurger = new FoodModel();
        mushroomBurger.setName("Mashroom Burger");
        mushroomBurger.setPrice(20000);
        mushroomBurger.setImage("https://food.fnr.sndimg.com/content/dam/images/food/fullset/2013/5/8/0/FN_Melissa-DArabian-Burger_s4x3.jpg.rend.hgtvcom.616.462.suffix/1371616351111.jpeg");
        pizza.setName("pizza");
        pizza.setPrice(25000);
        pizza.setImage("https://media-cdn.tripadvisor.com/media/photo-s/07/13/66/3b/gianni-s-ny-pizza.jpg");
        FoodModel fesenjoon = new FoodModel();
        fesenjoon.setName("fesenjan");
        fesenjoon.setPrice(50000);
        fesenjoon.setImage("https://fae-magazine.com/wp-content/uploads/2015/11/Fesenjan-31.jpg");
        List<FoodModel> foods = new ArrayList<>();
        foods.add(pizza);
        foods.add(fesenjan);
        foods.add(fesenjoon);
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(pizza);
        foods.add(fesenjoon);
        foods.add(fesenjan);
        FoodsAdapter adapter = new FoodsAdapter(mContext, foods);
        listView.setAdapter(adapter);
    }

}
