package com.safzali.samplecodesession03.foodslistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.safzali.samplecodesession03.R;

import java.util.List;

public class FoodsAdapter extends BaseAdapter {
    Context mContext;
    List<FoodModel> foods;

    public FoodsAdapter(Context mContext, List<FoodModel> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return foods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_foods_list, parent, false);
        TextView foodName = v.findViewById(R.id.food_name);
        TextView price = v.findViewById(R.id.food_price);
        ImageView image = v.findViewById(R.id.food_image);
        foodName.setText(foods.get(position).getName());
        price.setText(foods.get(position).getPrice() + "");
        Glide.with(mContext).load(foods.get(position).getImage()).into(image);
        return v;
    }
}
