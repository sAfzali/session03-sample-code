package com.safzali.samplecodesession03.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.orhanobut.hawk.Hawk;
import com.safzali.samplecodesession03.R;

public class BaseActivity extends AppCompatActivity {
    public Context mContext;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        Hawk.init(this).build();
    }
}
